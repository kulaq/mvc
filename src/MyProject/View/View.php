<?php

namespace MyProject\View;

class View
{
    public function __construct(
        private string $templatesPath,
    ) {}

    public function renderHtml(string $templateName, array $vars = [])
    {
        extract($vars);

        ob_start();
        include $this->templatesPath . '/' . $templateName;
        $buffer = ob_get_contents();
        ob_end_clean();

        echo $buffer;
    }

}