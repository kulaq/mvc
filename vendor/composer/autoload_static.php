<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitdddc9c8193e78467cc3e6c26b1893f17
{
    public static $files = array (
        'a067a2e425451d3ccc69a56bd40af347' => __DIR__ . '/../..' . '/www/index.php',
        'd511a130a568d952946b0a39d87cc34c' => __DIR__ . '/../..' . '/src/settings.php',
    );

    public static $prefixLengthsPsr4 = array (
        'M' => 
        array (
            'MyProject\\' => 10,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'MyProject\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src/MyProject',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitdddc9c8193e78467cc3e6c26b1893f17::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitdddc9c8193e78467cc3e6c26b1893f17::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInitdddc9c8193e78467cc3e6c26b1893f17::$classMap;

        }, null, ClassLoader::class);
    }
}
